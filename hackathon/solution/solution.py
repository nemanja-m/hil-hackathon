"""This module is main module for contestant's solution."""
from collections import OrderedDict
import os
import torch

from hackathon.framework.http_server import prepare_dot_dir
from hackathon.utils.control import Control
from hackathon.utils.utils import ResultsMessage, DataMessage, config_outs
from hackathon.solution.vaders.a2c.actor_critic import ActorCritic
from hackathon.solution.vaders.actions import get_action, action_to_results_message


def data_message_to_dict(data_msg):
    return OrderedDict({
        'gridStatus': float(data_msg.grid_status),
        'buyingPrice': float(data_msg.buying_price),
        'sellingPrice': float(data_msg.selling_price),
        'currentLoad': float(data_msg.current_load),
        'solarProduction': float(data_msg.solar_production),
        'bessSOC': float(data_msg.bessSOC),
        'bessOverload': float(data_msg.bessOverload),
        'mainGridPower': float(data_msg.mainGridPower),
        'bessPower': float(data_msg.bessPower)
    })


def sample_action(data_msg, net):
    data_msg_dict = data_message_to_dict(data_msg)
    state_tensor = torch.tensor(list(data_msg_dict.values()), dtype=torch.float32)
    probs, _, _ = net(state_tensor)
    action_idx = probs.argmax().item()
    [action] = get_action([action_idx])
    return action_to_results_message(action.values(), data_msg)


def worker(msg: DataMessage, net) -> ResultsMessage:
    """TODO: This function should be implemented by contestants."""
    return sample_action(msg, net)


def run(args) -> None:
    prepare_dot_dir()
    config_outs(args, 'solution')

    cntrl = Control()

    net = ActorCritic(9, 336)

    path = os.path.dirname(os.path.abspath(__file__))
    model_path = os.path.join(path, 'models/model_25')

    net.load_state_dict(torch.load(model_path))

    for data in cntrl.get_data():
        cntrl.push_results(worker(data, net))
