# Installation

- Clone `Typhoon HIL Hackathon` [repo](https://github.com/SrdjanPaunovic/Hackathon2017.git)
- Clone this repo in `Hackathon2017/`
- Run test with `python -m vaders.run`
