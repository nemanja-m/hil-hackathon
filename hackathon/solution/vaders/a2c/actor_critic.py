import torch
import torch.nn as nn
import torch.nn.functional as F


class ActorCritic(nn.Module):

    def __init__(self, state_size, action_space_size):
        super(ActorCritic, self).__init__()

        self.linear1 = nn.Linear(state_size, 1024)
        self.linear2 = nn.Linear(1024, 512)
        self.policy = nn.Linear(512, action_space_size)
        self.value = nn.Linear(512, 1)

    def forward(self, x):
        linear_output1 = F.relu(self.linear1(x))
        linear_output2 = F.relu(self.linear2(linear_output1))
        policy_output = self.policy(linear_output2)
        value_output = self.value(linear_output2)

        probs = F.softmax(policy_output, dim=0)
        log_probs = F.log_softmax(policy_output, dim=0)
        return probs, log_probs, value_output


if __name__ == '__main__':
    net = ActorCritic(9, 320)
    x = torch.tensor([1, 2, 3, 4, 5, 6, 7, 8, 9]).float()
    print(net(x))
