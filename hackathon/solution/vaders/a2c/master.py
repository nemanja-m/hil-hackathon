import time
import multiprocessing
import torch
import numpy as np

from hackathon.solution.vaders.actions import get_action
from torch.optim import RMSprop
from hackathon.solution.vaders.a2c.actor_critic import ActorCritic
from hackathon.solution.vaders.environment import MultiprocessEnvironment
from hackathon.solution.vaders.a2c.memory import Memory
from hackathon.solution.vaders.actions import ACTION_SPACE
from torch.distributions.uniform import Uniform


class Master:

    GREEDY_EPS_DECAY = 0.99
    EPS = 0.15
    GRAD_CLIP = 0.5
    ENTROPY_COEF = 0.01
    VALUE_LOSS_COEF = 0.5

    def __init__(self, state_size, action_size, learning_rate):
        self.state_size = state_size
        self.action_size = action_size
        self.global_net = ActorCritic(state_size, action_size)
        self.optimizer = RMSprop(self.global_net.parameters(), lr=learning_rate, eps=1e-5)

    def train(self, num_episodes=250, steps_per_update=120, steps_per_episode=7200):
        worker_count = multiprocessing.cpu_count()
        mp_environment = MultiprocessEnvironment(num_process=worker_count)
        memory = Memory(steps_per_update,
                        action_space_size=len(ACTION_SPACE),
                        state_space_size=9,
                        cpu_count=worker_count)

        for episode in range(num_episodes):
            mp_environment.reset(episode)
            for update in range(steps_per_episode // steps_per_update):
                start = time.time()
                memory.clear()
                self.optimizer.zero_grad()

                for step in range(steps_per_update):
                    current_states = mp_environment.get_current_state()
                    probs, log_probs, value = self.global_net(current_states)

                    use_eps_greedy = np.random.rand() < self.EPS * self.GREEDY_EPS_DECAY ** (episode + 1) 
                    if use_eps_greedy:
                        action_indices = [np.random.randint(0, len(ACTION_SPACE)) for _ in range(worker_count)]
                    else:
                        action_indices = torch.argmax(probs, 0)

                    actions = get_action(action_indices)
                    states, rewards, dones = mp_environment.step(actions)
                    entropy = -(log_probs * probs).sum(-1)
                    memory.store(step, value.squeeze(1), log_probs, entropy, torch.from_numpy(rewards))

                expected_reward = memory.compute_expected_reward()
                advantages = expected_reward - memory.values[:-1]

                value_loss = advantages.pow(2).mean()
                policy_logs = memory.policy_logs[:-1].mean(2)
                policy_loss = -(torch.tensor(advantages.data) * policy_logs).mean()

                loss = self.VALUE_LOSS_COEF * value_loss + policy_loss - self.ENTROPY_COEF * memory.entropies.mean()
                loss.backward()

                torch.nn.utils.clip_grad_norm_(self.global_net.parameters(), self.GRAD_CLIP)
                self.optimizer.step()

                print('{}/{} loss: {} --> {:.2f}'.format(update, episode, loss, time.time() - start))

            if episode % 25 == 0:
                torch.save(self.global_net.state_dict(), './models/model_{}'.format(episode))


if __name__ == '__main__':
    master = Master(9, 336, 1e-4)
    master.train()
