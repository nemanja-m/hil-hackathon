import multiprocessing

import torch


class Memory:

    def __init__(self, num_steps, action_space_size, state_space_size, cpu_count=multiprocessing.cpu_count()):
        self.steps = num_steps
        self.action_space = action_space_size
        self.state_space = state_space_size
        self.cpu_count = cpu_count
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self._init_memory_tensors(self.steps, self.action_space, self.state_space)

    def store(self, step, value, policy_log, entropy, reward):
        self.values[step] = value
        self.rewards[step] = reward
        self.entropies[step] = entropy
        self.policy_logs[step] = policy_log

    def set_state_to_continue(self, state):
        self.state_to_continue = state

    def retrieve(self):
        return self.values, self.policy_logs, self.rewards, self.entropies

    def clear(self):
        self._init_memory_tensors(self.steps, self.action_space, self.state_space)

    def _init_memory_tensors(self, num_steps, action_space_size, state_space_size):
        self.values = torch.zeros((num_steps, self.cpu_count)).to(self.device)
        self.policy_logs = torch.zeros((num_steps, self.cpu_count, action_space_size)).to(self.device)
        self.rewards = torch.zeros((num_steps, self.cpu_count)).to(self.device)
        self.entropies = torch.zeros((num_steps, self.cpu_count)).to(self.device)
        self.state_to_continue = torch.zeros((1, state_space_size, self.cpu_count)).to(self.device)

    def compute_expected_reward(self, discount_factor=0.99):
        expected_value = self.values[-1]
        expected_rewards = torch.zeros((self.steps, self.cpu_count)).to(self.device)
        expected_rewards[-1] = expected_value
        for step in reversed(range(self.rewards.size(0) - 1)):
            expected_rewards[step] = self.rewards[step] + discount_factor * expected_rewards[step + 1]
        return expected_rewards[:-1]


if __name__ == '__main__':
    memory = Memory(15, 320, 9)
    memory.store(0, 5, 1, torch.zeros((320)), 10, 7)
