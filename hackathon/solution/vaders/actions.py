import itertools
from collections import OrderedDict

import numpy as np

from hackathon.utils.utils import PVMode, ResultsMessage


ON_OFF_SPACE = [0, 1]

REF_VALUE_MIN = -5.0
REF_VALUE_MAX = 5.5
REF_VALUE_STEP = 0.5
REF_VALUE_SPACE = np.arange(REF_VALUE_MIN, REF_VALUE_MAX, REF_VALUE_STEP).tolist()


ACTION_SPACE = list(itertools.product(*[ON_OFF_SPACE] * 4 + [REF_VALUE_SPACE]))


def get_action(action_indices):
    actions = []
    for index in action_indices:
        load_one, load_two, load_three, pv_mode, power_ref = ACTION_SPACE[index]
        actions.append(OrderedDict([
            ('load_one', load_one),
            ('load_two', load_two),
            ('load_three', load_three),
            ('pv_mode', pv_mode),
            ('power_reference', power_ref)
        ]))
    return actions


def action_to_results_message(action, data_message):
    load_one, load_two, load_three, pv_mode, power_ref = action
    return ResultsMessage(data_msg=data_message,
                          load_one=bool(load_one),
                          load_two=bool(load_two),
                          load_three=bool(load_three),
                          power_reference=float(power_ref),
                          pv_mode=PVMode.ON if pv_mode else PVMode.OFF)
