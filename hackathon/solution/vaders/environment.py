import multiprocessing
from collections import OrderedDict

import numpy as np
import torch

from hackathon.utils.utils import DataMessage, ResultsMessage
from hackathon.solution.vaders.profile import generate_profile
from hackathon.solution.vaders.scoring import Scorer


class Environment:

    INITIAL_PHYSICS_STATE = {
        'bessSOC': 0.5,
        'bessOverload': False,
        'mainGridPower': 0,
        'bessPower': 0
    }

    def __init__(self, env_id):
        # Env id will be used in multiprocess environements
        self.env_id = env_id
        self._generate_profiles()

    def step(self, action):
        prev_data_msg = self._state_to_data_message(self.current_state,
                                                    message_id=self._current_step - 1)

        results_msg = self._action_to_result_message(action,
                                                     data_message=prev_data_msg)
        (energy_cost,
         main_grid_power,
         penal,
         bess_soc,
         is_bess_overloaded,
         bess_power) = self._scorer.get_physics_metrics(prev_data_msg, results_msg)

        reward = (energy_cost + penal)

        # Part of state defined by profile and is action independent
        profile_state = self.profile[self._current_step]

        # Part of state dependent on our actions
        action_state = {
            'bessSOC': bess_soc,
            'bessOverload': is_bess_overloaded,
            'mainGridPower': main_grid_power,
            'bessPower': bess_power
        }

        observation = {**profile_state, **action_state}
        self.current_state = OrderedDict(observation)

        done = self._current_step == self.total_steps - 1
        self._current_step += 1
        return observation, reward, done

    def reset(self, episode=0):
        """Returns initial state"""
        self.profile = self._profiles_per_episode[episode]

        # Length of one episode (we need to discuss this)
        self.total_steps = len(self.profile)

        # Thread-safe reward scorer
        self._scorer = Scorer()

        self._current_step = 1

        state = self._initial_state()
        self.current_state = state
        return self.current_state_as_tensor()

    def current_state_as_tensor(self):
        return torch.tensor(list(self.current_state.values()), dtype=torch.float32)

    def _generate_profiles(self, num_episodes=75):
        """Generate profiles for each episode.

        Solves problem with random seed between processes.
        """
        self._profiles_per_episode = []
        for episode in range(num_episodes):
            profile = generate_profile(days=5)
            self._profiles_per_episode.append(profile)

    def _initial_state(self):
        profile_state = self.profile[0]
        initial_state = OrderedDict({**profile_state, **self.INITIAL_PHYSICS_STATE})
        return initial_state

    def _action_to_result_message(self, action, data_message):
        return ResultsMessage(data_msg=data_message,
                              load_one=action['load_one'],
                              load_two=action['load_two'],
                              load_three=action['load_three'],
                              power_reference=action['power_reference'],
                              pv_mode=action['pv_mode'])

    def _state_to_data_message(self, state, message_id):
        return DataMessage(id=message_id,
                           grid_status=state['gridStatus'],
                           buying_price=state['buyingPrice'],
                           selling_price=state['sellingPrice'],
                           current_max_load=state['currentLoad'],
                           solar_production=state['solarProduction'],
                           bessSOC=state['bessSOC'],
                           bessOverload=state['bessOverload'],
                           mainGridPower=state['mainGridPower'],
                           bessPower=state['bessPower'])


ENVIRONMENTS = [Environment(env_id) for env_id in range(multiprocessing.cpu_count())]


def env_worker(pid, connection):
    env = ENVIRONMENTS[pid]

    while True:
        message, data = connection.recv()

        if message == 'step':
            state, reward, done = env.step(data)
            if done:
                state = env.reset()
            connection.send((state, reward, done))
        elif message == 'reset':
            state = env.reset(data)
            connection.send(state)
        elif message == 'get_current_state':
            state = env.current_state_as_tensor()
            connection.send(state)
        elif message == 'close':
            connection.close()
            break
        else:
            raise NotImplementedError()


class MultiprocessEnvironment:

    def __init__(self, num_process):
        self.is_closed = False
        self.parent_conns, self.child_conns = zip(*[multiprocessing.Pipe()
                                                    for _ in range(num_process)])
        self.workers = [
            multiprocessing.Process(target=env_worker,
                                    daemon=True,
                                    args=(pid, child_conn))
            for pid, child_conn in enumerate(self.child_conns)
        ]

        for worker in self.workers:
            worker.start()

    def step(self, actions):
        for worker, action in zip(self.parent_conns, actions):
            worker.send(('step', action))

        results = [worker.recv() for worker in self.parent_conns]
        states, rewards, dones = zip(*results)
        return np.stack(states), np.stack(rewards), np.stack(dones)

    def get_current_state(self):
        for worker in self.parent_conns:
            worker.send(('get_current_state', None))

        states = [worker.recv() for worker in self.parent_conns]
        return torch.stack(states)

    def reset(self, episode):
        for worker in self.parent_conns:
            worker.send(('reset', episode))
        return torch.stack([worker.recv() for worker in self.parent_conns])

    def close(self):
        if self.is_closed:
            return

        for conn in self.parent_conns:
            conn.send(('close', None))

        for worker in self.workers:
            worker.join()

        self.is_closed = True


if __name__ == '__main__':
    from vaders.actions import get_action

    num_process = multiprocessing.cpu_count()
    menv = MultiprocessEnvironment(num_process)

    states, rewards, dones = menv.step([get_action(42) for _ in range(num_process)])
    menv.close()
    print('Rewards: {}'.format(rewards))
