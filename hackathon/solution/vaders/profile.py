from typing import List, Tuple

import numpy as np

from hackathon.energy.energy_math import gen_profile


SAMPLE_RATE = 60  # Minutes


def sample_scales(mean: float, std: float, days: int) -> List[float]:
    """Return scale samples from normal distribution"""
    return [round(x, 2) for x in np.random.normal(loc=mean, scale=std, size=(days))]


def sample_blackouts(days: int,
                     blackout_probability: float = 0.65,
                     full_backout_probability: float = 0.075) -> List[Tuple[float, float]]:
    """Return blackout time intervals."""
    start_upper_limit = 2376  # 23:45, max value for blackout start time
    end_upper_limit = 2401  # 00:00, max value for blackout end time

    blackouts = []
    for _ in range(days):
        is_blackout = np.random.rand() > 1 - blackout_probability
        if is_blackout:
            is_full_hour_blackout = np.random.rand() > 1 - full_backout_probability
            if is_full_hour_blackout:
                start = np.random.randint(0, 2301)
                end = start + 100
            else:
                start = np.random.randint(0, start_upper_limit)
                end = np.random.randint(start, start + 100)
                end = end if end < end_upper_limit else end_upper_limit - 1
            blackout = [(start / 100, end / 100)]
        else:
            blackout = []
        blackouts.append(blackout)
    return blackouts


def generate_profile(days: int = 5, sample_rate: int = SAMPLE_RATE) -> List[dict]:
    load_scales = sample_scales(mean=1.0, std=0.2, days=5)
    solar_scales = sample_scales(mean=1.0, std=0.5, days=5)
    blackouts = sample_blackouts(days=5)

    # Used to smoothen out the load PROFILES on day transitions
    load_scaling_prev = 1.0

    profiles = []
    for day in range(5):
        _, profile = gen_profile(sample_rate,
                                 load_scaling=load_scales[day],
                                 load_scaling_prev=load_scaling_prev,
                                 solar_scaling=solar_scales[day],
                                 blackouts=blackouts[day])
        profiles.extend(profile)
        load_scaling_prev = load_scales[day]
    return profiles
