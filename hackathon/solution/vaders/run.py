import multiprocessing

import numpy as np

from hackathon.solution.vaders.environment import Environment


# Hard coded action for demo purpose
action = {
    'load_one': True,
    'load_two': True,
    'load_three': True,
    'pv_mode': True,
    'power_reference': 5.0,
}


def worker(pid, seed):
    np.random.seed(seed)

    penalities = []
    is_done = False
    env = Environment(env_id=pid)
    env.reset()

    while not is_done:
        observation, penality, done = env.step(action)
        penalities.append(penality)
        is_done = done

    total_reward = sum(penalities)
    return pid, total_reward


if __name__ == '__main__':
    cpu_count = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(cpu_count)
    params = [(pid, np.random.randint(100)) for pid in range(cpu_count)]
    returns = pool.starmap(worker, params)

    for pid, reward in returns:
        print('pid {}\treward: {:.2f}'.format(pid, reward))
