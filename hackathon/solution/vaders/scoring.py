from typing import Tuple

import hackathon.energy.rating as hil_scorer
from hackathon.utils.utils import DataMessage, PVMode, ResultsMessage, CFG


BESS_MAX_POWER = 5
BESS_CAPACITY = 20


class Scorer:
    """Thread-safe physic metric scorer.

    This class fix problem with global variables in `hackathon.energy.scorer`
    module and enables multiprocess environment workers. Irrelevant input
    arguments and return values are removed also.

    """

    def __init__(self):
        self._overload_cnt = 0
        self._penal_l1_cnt = 0
        self._penal_l2_cnt = 0

    def get_physics_metrics(self,
                            data_msg: DataMessage,
                            results_msg: ResultsMessage) -> Tuple[float, float, float, float, bool, float]:
        penal = 0.0

        if results_msg.power_reference > BESS_MAX_POWER:
            results_msg.power_reference = BESS_MAX_POWER
        elif results_msg.power_reference < -BESS_MAX_POWER:
            results_msg.power_reference = -BESS_MAX_POWER

        if not results_msg.load_one:
            if self._penal_l1_cnt == 0:
                penal += hil_scorer.PENAL_L1_INIT + hil_scorer.PENAL_L1_CONT
                self._penal_l1_cnt += 1
            else:
                penal += hil_scorer.PENAL_L1_CONT
        else:
            self._penal_l1_cnt = 0

        if not results_msg.load_two:
            if self._penal_l2_cnt == 0:
                penal += hil_scorer.PENAL_L2_INIT + hil_scorer.PENAL_L2_CONT
                self._penal_l2_cnt += 1
            else:
                penal += hil_scorer.PENAL_L2_CONT
        else:
            self._penal_l2_cnt = 0

        if not results_msg.load_three:
            penal += hil_scorer.PENAL_L3_CONT

        if data_msg.grid_status:
            if (data_msg.bessSOC == 0 and results_msg.power_reference > 0) \
               or (data_msg.bessSOC == 1 and results_msg.power_reference < 0):
                results_msg.power_reference = 0

            r_load = hil_scorer.real_load(int(results_msg.load_one), int(results_msg.load_two),
                                          int(results_msg.load_three), data_msg.current_load)

            mg = hil_scorer.main_grid(True, r_load, results_msg.power_reference,
                                      data_msg.solar_production, results_msg.pv_mode)
            # we sell
            if mg < 0:
                bess_sell = abs(mg) * data_msg.selling_price / CFG.sampleRate
                consumption = 0.0
            else:
                consumption = mg * data_msg.buying_price / CFG.sampleRate
                bess_sell = 0

            current_power = results_msg.power_reference

            soc_bess = data_msg.bessSOC - results_msg.power_reference / (CFG.sampleRate * BESS_CAPACITY)

            overload = False
        elif not data_msg.grid_status:
            r_load = hil_scorer.real_load(int(results_msg.load_one), int(results_msg.load_two),
                                          int(results_msg.load_three), data_msg.current_load)

            current_power = hil_scorer.main_grid(False, r_load, results_msg.power_reference,
                                                 data_msg.solar_production, results_msg.pv_mode)

            soc_bess = data_msg.bessSOC - current_power / (CFG.sampleRate * BESS_CAPACITY)

            if abs(current_power) > BESS_MAX_POWER or (soc_bess >= 1 and current_power < 0) \
               or (soc_bess <= 0 and current_power > 0):
                overload = True
                self._overload_cnt += 1
            else:
                overload = False
                self._overload_cnt = 0

            if self._overload_cnt > 1:
                penal = hil_scorer.PENAL_L1_INIT + hil_scorer.PENAL_L1_CONT + \
                    hil_scorer.PENAL_L2_INIT + hil_scorer.PENAL_L2_CONT + hil_scorer.PENAL_L3_CONT
                current_power = 0
                results_msg.load_one = False
                results_msg.load_two = False
                results_msg.load_three = False
                results_msg.pv_mode = PVMode.OFF
                overload = False
                self._overload_cnt = 0
                soc_bess = data_msg.bessSOC
                r_load = 0

            consumption = 0
            mg = 0
            bess_sell = 0

        if 0 > soc_bess:
            soc_bess = 0
        if soc_bess > 1:
            soc_bess = 1

        em = hil_scorer.energy_mark(consumption, bess_sell)
        return em, mg, penal, soc_bess, overload, current_power
